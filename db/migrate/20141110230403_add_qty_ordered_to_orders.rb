class AddQtyOrderedToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :qty_ordered, :integer, default: 1
  end
end
