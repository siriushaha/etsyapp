class AddQtyinstockToListings < ActiveRecord::Migration
  def change
    add_column :listings, :qty_in_stock, :integer, default: 1
  end
end
