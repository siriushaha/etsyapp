class AddListingIdToReviews < ActiveRecord::Migration
  def change
    remove_column :reviews, :restaurant_id
    add_column :reviews, :listing_id, :integer
  end
end
