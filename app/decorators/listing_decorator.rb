class ListingDecorator
  attr_reader :listing
  
  def initialize(listing)
    @listing = listing
  end  
  
  def method_missing(method_name, *args, &block)
    listing.send(method_name, *args, &block)
  end
  
  def respond_to_missing?(method_name, include_private = false)
    listing.respond_to?(method_name, include_private) || super
  end 

   def display_in_stock
     in_stock? ? "Only #{listing.qty_in_stock} left in stock" : "Not in stock"
  end
  
  def in_stock?
    listing.qty_in_stock > 0
  end

end
