class Order < ActiveRecord::Base
  
  validates :address, :city, :state, presence: true

  belongs_to :listing
  belongs_to :buyer, class_name: "User"
  belongs_to :seller, class_name: "User"
  validates :qty_ordered, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0,
    message: "can only be positive greater than 0"
  }

  def address_display
    address = "#{self.address}<br />"
    address += "#{self.city}, #{self.state} #{self.zipcode}<br />"
    address
  end

  scope :recent, -> { order(created_at: :desc) }
  scope :sold_to_seller, -> (seller) { where(seller: seller) }
  scope :purchased_by_buyer, -> (buyer) { where(buyer: buyer) }
end
