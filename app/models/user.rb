class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :timeoutable, :timeout_in => 15.minutes
  
  validates :name, presence: true
  
  has_many :listings, dependent: :destroy
  has_many :sales, class_name: "Order", foreign_key: "seller_id"
  has_many :purchases, class_name: "Order", foreign_key: "buyer_id"
  has_many :reviews, dependent: :destroy
  
  scope :recent, -> { order(created_at: :desc) }
  scope :excluding_user_by_email, -> (user_email) { where.not(email: user_email) }
end
