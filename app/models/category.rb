class Category < ActiveRecord::Base
  validates :category, presence: true 
  validates_uniqueness_of :category, message: "%{value} already exists."
  has_many :listings
  
  scope :recent, -> { order(created_at: :desc) }
  scope :active, -> { where(active: true) }
  scope :sorted, -> { order(:category) }
end
