class Listing < ActiveRecord::Base
  if Rails.env.development?
    has_attached_file :image, :styles => { :medium => "200x", :thumb => "100x100>" }, :default_url => "default.jpg"
  else
    has_attached_file :image, :styles => { :medium => "200x", :thumb => "100x100>" }, :default_url => "default.jpg",
        :storage => :dropbox,
        :dropbox_credentials => Rails.root.join("config/dropbox.yml"),
        :path => ":style/:id_:filename"
  end

  validates :name, :description, :price, :category_id, presence: true
  validates :price, numericality: { greater_than: 0 }
#  validates_attachment_presence :image
  validates :qty_in_stock, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0,
    message: "can only be positive greater than 0"
  }
  validates_attachment_content_type :image, :content_type => /\Aimage/
  
  belongs_to :user
  belongs_to :category
  has_many :orders
  has_many :reviews
  
  searchkick
  
  scope :active, -> { where(published: true) }
  scope :inactive, -> { where(published: false)}
  scope :recent, -> { order(created_at: :desc) }
  scope :by_logged_user, -> (logged_user) { where(user: logged_user)}
  scope :by_category, -> (category) { where(category_id: category) }
end
