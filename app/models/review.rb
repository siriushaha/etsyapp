class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :listing
  
  validates :rating, :comment, presence: true
  validates :rating, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 1,
    less_than_or_equal_to: 5,
    message: "can only be a whole number between 1 and 5"
  }
  
  scope :recent, -> { order(created_at: :desc) }
  scope :by_listing, -> (listing_id) { where(listing_id: listing_id) }
end
