class StripeService
  
  def initialize()
    Stripe.api_key = ENV["STRIPE_API_KEY"]    
  end
  
  def charge_payment_to_buyer(token, total,flash)  
    begin
      charge = Stripe::Charge.create(amount:  (total * 100).floor, currency: "usd", card: token) 
    rescue Stripe::CardError => e
      flash[:alert] = e.message
    end
  end  
  
  def transfer_payment_to_seller(recipient, total,flash)
    begin
      transfer = Stripe::Transfer.create(amount: (total * 95).floor, currency:  "usd", recipient: recipient)
    rescue Stripe::CardError => e
      flash[:alert] = e.message
    end
  end
  
  def create_recipient(token, name,flash)
    begin
      recipient = Stripe::Recipient.create(name: name, type:  "individual", bank_account: token)   
    rescue Stripe::CardError => e
      flash[:alert] = e.message
    end
  end
  
end
