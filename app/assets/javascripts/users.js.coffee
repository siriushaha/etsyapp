# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#  Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'))
#  listing.setupForm()
jQuery ->
  Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'))
  recipient.setupForm()

recipient =
  setupForm: ->
    $('#new_recipient').submit ->
      #if $('input').length > 6
        $('input[type=submit]').attr('disabled', true)
        Stripe.bankAccount.createToken($('#new_recipient'), recipient.handleStripeResponse)
        false

  handleStripeResponse: (status, response) ->
    if status == 200
      $('#new_recipient').append($('<input type="hidden" name="stripeToken" />').val(response.id))
      $('#new_recipient')[0].submit()
    else
      $('#stripe_error').text(response.error.message).show()
      $('input[type=submit]').attr('disabled', false)