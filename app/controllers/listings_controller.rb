class ListingsController < ApplicationController
   
  before_action :set_listing, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:seller, :new, :create, :edit, :update, :destroy]
  before_action :check_user, only: [:edit, :update, :destroy]
  
  def search
    if params[:search].present?
      @listings = Listing.search params[:search], where: {published: true}, page: params[:page], per_page: Paginable::LISTING_INDEX_MAX_PER_PAGE
    else
      @listings = Listing.active.recent.paginate(pagination_size(Paginable::LISTING_INDEX_MAX_PER_PAGE))
    end 
  end
  
  def seller
    @listings = Listing.by_logged_user(current_user).recent.paginate(pagination_size(Paginable::LISTING_SELLER_MAX_PER_PAGE))
  end
  
  # GET /listings
  # GET /listings.json
  def index
    if params[:category].blank?
      @listings = Listing.active.recent.paginate(pagination_size(Paginable::LISTING_INDEX_MAX_PER_PAGE))
    else
      category_id = Category.find_by_category(params[:category]).id
      @listings = Listing.by_category(category_id).active.recent.paginate(pagination_size(Paginable::LISTING_INDEX_MAX_PER_PAGE))
    end
  end

  # GET /listings/1
  # GET /listings/1.json
  def show
    @reviews = Review.by_listing(@listing.id).recent
    if @reviews.blank?
      @avg_rating = 0
    else
      @avg_rating = @reviews.average(:rating).round(2)
    end
  end

  # GET /listings/new
  def new
    @listing = Listing.new
  end

  # GET /listings/1/edit
  def edit
  end

  # POST /listings
  # POST /listings.json
  def create
    @listing = Listing.new(listing_params)
    @listing.user_id = current_user.id
 
    respond_to do |format|
      if @listing.save
        format.html { redirect_to @listing, notice: 'Listing was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /listings/1
  # PATCH/PUT /listings/1.json
  def update
    respond_to do |format|
      if @listing.update(listing_params)
        format.html { redirect_to @listing, notice: 'Listing was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /listings/1
  # DELETE /listings/1.json
  def destroy
    @listing.destroy
    respond_to do |format|
      format.html { redirect_to listings_url, notice: 'Listing was successfully removed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_listing
      @listing = Listing.find(params[:id])
      @listing_decorator = ListingDecorator.new(@listing)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def listing_params
      params.require(:listing).permit(:name, :category_id, :description, :price, :qty_in_stock, :image, :published)
    end

    def check_user
      if current_user != @listing.user
        redirect_to root_url, alert: "Sorry, this listing product belongs to someone else"
      end
    end
  
end
