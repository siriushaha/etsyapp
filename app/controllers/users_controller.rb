class UsersController < ApplicationController
  before_action :set_user, only: [:edit, :update]
  before_action :set_user_recipient, only: [:recipient, :update_recipient]
  
  # GET /users
  # GET /users.json
  def index
    @users = User.excluding_user_by_email(current_user.email).recent.paginate(pagination_size(Paginable::USER_SIZE_MAX_PAGE))
  end


  # GET /users/1/edit
  def edit
  end


  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to users_path }
      else
        format.html { render :edit }
      end
    end
  end

  # GET /recipient
  def recipient    
  end


  # PATCH/PUT /update_recipient
  # PATCH/PUT /users/1.json
  def update_recipient
    stripe_service = StripeService.new
    recipient = stripe_service.create_recipient(params[:stripeToken], current_user.name, flash)
    @user.recipient = recipient.id
    
    respond_to do |format|
      if @user.save
        format.html { redirect_to listings_path }
      else
        format.html { render :recipient }
      end
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :admin, :country, :routing_number, :account_number)
    end
  
    def set_user_recipient
      @user = current_user
    end
  
end
