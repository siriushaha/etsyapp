class OrdersController < ApplicationController
  #before_action :set_order, only: [:show, :edit, :update, :destroy]
  before_action :set_listing, only: [:new, :create]
  before_action :set_order, only: [:receipt]
  before_action :authenticate_user!
  before_action :check_user, only: [:new]
  before_action :check_seller_recipient, only: [:create]
  before_action :check_qty_in_stock, only: [:create]
  
  def sales
    @orders = Order.sold_to_seller(current_user).recent.paginate(pagination_size(Paginable::ORDER_HISTORY_MAX_PER_PAGE))
  end

  def purchases
    @orders = Order.purchased_by_buyer(current_user).recent.paginate(pagination_size(Paginable::ORDER_HISTORY_MAX_PER_PAGE))
  end
  
  # GET /orders/new
  def new
    @order = Order.new
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)
    @seller = @listing.user
    
    @order.listing_id = @listing.id
    @order.buyer_id = current_user.id
    @order.seller_id = @seller.id

    @order.total_amount = @order.qty_ordered * @listing.price
    @listing.qty_in_stock -= @order.qty_ordered if @listing.qty_in_stock > 0

    stripe_service = StripeService.new
    charge = stripe_service.charge_payment_to_buyer(params[:stripeToken], @order.total_amount, flash)
    transfer = stripe_service.transfer_payment_to_seller(@seller.recipient, @order.total_amount, flash)    
       
    flash[:notice] = "Thanks for your order."
    
    respond_to do |format|
      if @order.save && @listing.save
        format.html { redirect_to receipt_url(@order.id) }
      else
        format.html { render :new }
      end
    end
  end

  def receipt
    @user = User.find(@order.buyer_id)
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:address, :city, :state, :zipcode, :qty_ordered)
    end
  
    def set_listing
      @listing = Listing.find(params[:listing_id])
    end
  
    def check_user
      if @listing.user == current_user
        redirect_to root_url, alert: "Sorry, you cannot buy your own listing product."
      end
    end
  
    def check_seller_recipient
      if @listing.user.recipient.blank?
        redirect_to recipient_url, alert: "Sorry, bank recipient is not setup for this listing seller"
      end
    end
  
    def check_qty_in_stock
      parms = order_params
      unless parms[:qty_ordered].to_i <= @listing.qty_in_stock
        redirect_to new_listing_order_path(@listing), alert: "Quantity ordered was more than #{@listing.qty_in_stock} items left in stock. Please adjust quantity ordered."
      end
    end
end
