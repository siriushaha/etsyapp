module Paginable
  extend ActiveSupport::Concern
  
  included do
    LISTING_SELLER_MAX_PER_PAGE = 10
    LISTING_INDEX_MAX_PER_PAGE = 4
    ORDER_HISTORY_MAX_PER_PAGE = 10 
    CATEGORY_SIZE_MAX_PAGE = 10
    USER_SIZE_MAX_PAGE = 10
  end
  
  private
  
    def pagination_size(max)
      {page: params[:page], per_page: max}
    end
  
end